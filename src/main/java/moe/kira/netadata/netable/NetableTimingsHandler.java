package moe.kira.netadata.netable;

import java.util.Map;

import com.google.common.collect.Maps;

public class NetableTimingsHandler extends org.spigotmc.CustomTimingsHandler {
    public final Map<String, Object> netadataValues = Maps.newHashMap();
    
    public NetableTimingsHandler(Class<?> entityType) {
        super("** tickEntity - " + entityType.getSimpleName()); // current name format using in SpigotTimings
    }

    public NetableTimingsHandler(Class<?> entityType, String initKey, Object initValue) {
        this(entityType);
        netadataValues.put(initKey, initValue);
    }
    
}
