package moe.kira.netadata.netable;

import java.util.Map;

import org.bukkit.Location;

import com.google.common.collect.Maps;

public class NetableLocation extends org.bukkit.Location {
    public final Map<String, Object> netadataValues = Maps.newHashMap();

    public NetableLocation(Location origin) {
        super(origin.getWorld(), origin.getX(), origin.getY(), origin.getZ());
    }
    
    public NetableLocation(Location origin, String initKey, Object initValue) {
        this(origin);
        netadataValues.put(initKey, initValue);
    }
}
