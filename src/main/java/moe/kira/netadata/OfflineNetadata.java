package moe.kira.netadata;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Maps;

public class OfflineNetadata implements Listener {
    protected static final Map<String, Map<String, Object>> offlineNetadata = Maps.newHashMap();
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void restore(PlayerLoginEvent evt) {
        String key = evt.getPlayer().getName().toLowerCase(Locale.ROOT);
        Map<String, Object> netadata = offlineNetadata.get(key);
        if (netadata != null) Netadata.accessor.getMap(evt.getPlayer()).putAll(netadata);
        offlineNetadata.remove(key);
        key = null; netadata = null; // you go
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void restore(PlayerQuitEvent evt) {
        String key = evt.getPlayer().getName().toLowerCase(Locale.ROOT);
        Map<String, Object> netadata = Netadata.accessor.getMapUnsafe(evt.getPlayer());
        if (netadata != Collections.EMPTY_MAP) offlineNetadata.put(key, netadata);
        key = null; netadata = null; // you go
    }
}
