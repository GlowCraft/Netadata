package moe.kira.netadata;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import org.bukkit.entity.Entity;

@NotThreadSafe
@SuppressWarnings("unchecked")
public class Netadata <V> implements Map<String, V> {
    protected static NetadataAccessor accessor;
    private Entity bind;
    
    public Netadata(Entity entity) {
        bind = entity;
    }
    
    /*
     * Netadata access
     */
    public Netadata<V> to(Entity entity) {
        bind = entity;
        return this;
    }
    
    public static <V> Netadata<V> link(Entity entity) {
        return new Netadata<V>(entity);
    }
    
    /*
     * Static access
     */
    public static boolean has(String key, Entity entity) {
        return accessor.getMapUnsafe(entity).containsKey(key);
    }
    
    @Nullable
    public static NetadataValue get(String key, Entity entity) {
        Map<String, Object> map = accessor.getMapUnsafe(entity);
        Object value = map.get(key);
        return map == Collections.EMPTY_MAP ? null : value instanceof NetadataValue ? (NetadataValue) value : NetadataValue.wrap(value);
    }
    
    @Nullable
    public static NetadataValue remove(String key, Entity entity) {
        Map<String, Object> map = accessor.getMapUnsafe(entity);
        Object value = map.remove(key);
        return map == Collections.EMPTY_MAP ? null : value instanceof NetadataValue ? (NetadataValue) value : NetadataValue.wrap(value);
    }
    
    public static void remove(String key, Entity... entity) {
        for (Entity e : entity) accessor.getMapUnsafe(e).remove(key);
    }
    
    public static List<Entity> remove(String key, List<Entity> entity) {
        for (Entity e : entity) accessor.getMapUnsafe(e).remove(key);
        return entity;
    }
    
    @Nullable
    public static NetadataValue set(String key, Object value, Entity entity) {
        Object prev = accessor.getMap(entity).put(key, value); // safe!
        return prev == null ? null : prev instanceof NetadataValue ? (NetadataValue) prev : NetadataValue.wrap(prev);
    }
    
    public static void set(String key, Object value, Entity... entity) {
        for (Entity e : entity) accessor.getMap(e).put(key, value); // safe!
    }
    
    public static List<Entity> set(String key, Object value, List<Entity> entity) {
        for (Entity e : entity) accessor.getMap(e).put(key, value); // safe!
        return entity;
    }
    
    /*
     * Map implementation
     */
    @Override
    public int size() {
        return accessor.getMapUnsafe(bind).size();
    }

    @Override
    public boolean isEmpty() {
        return accessor.getMapUnsafe(bind).isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return accessor.getMapUnsafe(bind).containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return accessor.getMapUnsafe(bind).containsValue(value);
    }

    @Override
    public V get(Object key) {
        return (V) accessor.getMapUnsafe(bind).get(key);
    }

    @Override
    public V put(String key, V value) {
        return (V) accessor.getMap(bind).put(key, value); // safe!
    }

    @Override
    public V remove(Object key) {
        return (V) accessor.getMapUnsafe(bind).remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends V> m) {
        accessor.getMap(bind).putAll(m); // safe!
    }

    @Override
    public void clear() {
        accessor.getMapUnsafe(bind).clear();
    }

    @Override
    public Set<String> keySet() {
        return accessor.getMapUnsafe(bind).keySet();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Collection<V> values() {
        return (Collection) accessor.getMapUnsafe(bind).values();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Set<Entry<String, V>> entrySet() {
        return (Set) accessor.getMapUnsafe(bind).entrySet();
    }
 
}
