package moe.kira.netadata;

import java.util.Map;

import org.bukkit.entity.Entity;

public interface NetadataAccessor {
    public Map<String, Object> getMap(Entity entity);
    
    public Map<String, Object> getMapUnsafe(Entity entity);
}
