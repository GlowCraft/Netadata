package moe.kira.netadata.impl.v1_9_R2;

import java.util.Collections;
import java.util.Map;

import org.bukkit.entity.Entity;

import moe.kira.netadata.NetadataAccessor;
import moe.kira.netadata.netable.NetableTimingsHandler;

public class EntityAccessor implements NetadataAccessor {
    @Override
    public Map<String, Object> getMap(Entity entity) {
        net.minecraft.server.v1_9_R2.Entity nmsEntity = ((org.bukkit.craftbukkit.v1_9_R2.entity.CraftEntity) entity).getHandle();
        if (nmsEntity.tickTimer instanceof NetableTimingsHandler) {
            return ((NetableTimingsHandler) nmsEntity.tickTimer).netadataValues;
        } else {
            Class<?> entityType = nmsEntity.getClass();
            nmsEntity.tickTimer = new NetableTimingsHandler(entityType);
            // put new timer, make timing works!
            org.bukkit.craftbukkit.v1_9_R2.SpigotTimings.entityTypeTimingMap.put(entityType.getName(), nmsEntity.tickTimer);
            return ((NetableTimingsHandler) nmsEntity.tickTimer).netadataValues;
        }
    }
    
    @Override
    public Map<String, Object> getMapUnsafe(Entity entity) {
        net.minecraft.server.v1_9_R2.Entity nmsEntity = ((org.bukkit.craftbukkit.v1_9_R2.entity.CraftEntity) entity).getHandle();
        return nmsEntity.tickTimer instanceof NetableTimingsHandler ? ((NetableTimingsHandler) nmsEntity.tickTimer).netadataValues : Collections.<String, Object>emptyMap();
    }
    
}
