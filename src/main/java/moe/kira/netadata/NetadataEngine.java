package moe.kira.netadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import moe.kira.netadata.common.VersionLevel;

public class NetadataEngine extends JavaPlugin {
    static NetadataEngine instance;
    
    private void coffee(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException interrupt) {
            // life is so hard
        }
    }
    
    private void lookupAccessor() {
        instance = this;
        try {
            Class<?> impl = Class.forName("moe.kira.netadata.impl." + (VersionLevel.isPaper() ? "paper." : "") + VersionLevel.strip() + ".EntityAccessor");
            Netadata.accessor = (NetadataAccessor) impl.newInstance();
            Entity te = Bukkit.getWorld("world").spawnEntity(Bukkit.getWorlds().get(0).getSpawnLocation(), EntityType.PIG); // for test
            Netadata.accessor.getMap(te);
            te.remove(); te = null;
        } catch (Throwable t) {
            t.printStackTrace();
            Bukkit.getLogger().warning("NetadataEngine > 当前的版本不兼容 Netadata : " + VersionLevel.strip());
            coffee(10);
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public void onEnable() {
        lookupAccessor();
        
        Map<String, Map<String, Object>> worldNetadata = null;
        for (World world : Bukkit.getWorlds()) {
            File folder = world.getWorldFolder();
            File dataFile = new File(folder, "persistents.netadata");
            if (!dataFile.exists() || dataFile.isDirectory()) continue;
            
            Input: {
                try {
                    FileInputStream fis = new FileInputStream(dataFile);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    worldNetadata = (Map<String, Map<String, Object>>) ois.readObject();
                    fis.close(); ois.close();
                } catch (IOException io) {
                    io.printStackTrace();
                    Bukkit.getLogger().warning("NetadataEngine > 读取 Netadata 数据时遇到问题 : 正重试");
                    coffee(5);
                    break Input;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace(); // im so weak
                    Bukkit.getLogger().severe("NetadataEngine > 读取到了未知的数据类型");
                    coffee(5);
                    Bukkit.getPluginManager().disablePlugin(this);
                }
            }
            if (worldNetadata == null || worldNetadata.isEmpty()) continue;
            
            // migrate player netadata, simple put them and remove from here
            Iterator<Entry<String, Map<String, Object>>> it = worldNetadata.entrySet().iterator();
            Entry<String, Map<String, Object>> entry;
            while (it.hasNext()) {
                entry = it.next();
                if (!entry.getKey().startsWith("ID")) continue; // not player
                for (Entry<String, Object> se : entry.getValue().entrySet()) {
                    se.setValue(NetadataValue.wrap(se.getValue(), Neta.PERSISTENT)); // see you next time!
                }
                OfflineNetadata.offlineNetadata.put(entry.getKey().substring(2), entry.getValue()); // strip "ID"
                it.remove();
            }
            
            // restore netadata for existing entities
            Map<String, Object> netadata;
            for (Entity e : world.getEntities()) {
                if (e instanceof Player) continue; // striped before!
                String key = "" + e.getUniqueId().hashCode();
                if ((netadata = worldNetadata.get(key)) == null) continue;
                for (Entry<String, Object> subdata : netadata.entrySet()) {
                    Netadata.set(subdata.getKey(), NetadataValue.wrap(subdata.getValue(), Neta.PERSISTENT), e); // see you next time
                }
            }
        }
        worldNetadata = null;
        
        // restore offline netadata for online players
        Bukkit.getPluginManager().registerEvents(new OfflineNetadata(), this);
        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
        if (!OfflineNetadata.offlineNetadata.isEmpty() && !players.isEmpty()) {
            String key; Map<String, Object> data;
            for (Player player : players) {
                key = player.getName().toLowerCase(Locale.ROOT);
                data = OfflineNetadata.offlineNetadata.get(key);
                if (data != null) Netadata.accessor.getMap(player).putAll(data);
            }
            key = null; data = null; // you go
        }
        
        Bukkit.getLogger().warning("NetadataEngine > 已载入 : " + VersionLevel.strip());
    }
    
    @Override
    public void onDisable() {
        Map<String, Map<String, Object>> worldNetadata = Maps.newHashMap();
        for (World world : Bukkit.getWorlds()) {
            worldNetadata.clear();
            File folder = world.getWorldFolder();
            File dataFile = new File(folder, "persistents.netadata");
            if (dataFile.exists() && !dataFile.isDirectory()) dataFile.delete();
            
            // restore netadata for online entities/players
            Map<String, Object> entity_netadata;
            Iterator<Entry<String, Object>> it;
            for (Entity e : world.getEntities()) {
                if ((entity_netadata = Netadata.accessor.getMapUnsafe(e)) == Collections.EMPTY_MAP) continue; // non-netadata entity
                
                // strip entries
                it = entity_netadata.entrySet().iterator();
                Entry<String, Object> entity_netadata_entry;
                Loop_Entity: while (it.hasNext()) {
                    entity_netadata_entry = it.next();
                    if (!(entity_netadata_entry.getValue() instanceof NetadataValue)) {
                        it.remove();
                        continue;
                    }
                    Object realValue;
                    for (Neta neta : ((NetadataValue) entity_netadata_entry.getValue()).neta) {
                        if (neta != Neta.PERSISTENT) continue;
                        realValue = ((NetadataValue) entity_netadata_entry.getValue()).value;
                        if (!(realValue instanceof Serializable)) {
                            it.remove();
                            continue Loop_Entity;
                        }
                        entity_netadata_entry.setValue(realValue); // remember to re-wrap on loading
                        continue Loop_Entity;
                    }
                    it.remove(); // entry gets here is non-persistent
                }
                
                worldNetadata.put(e instanceof Player ? "ID" + e.getName().toLowerCase(Locale.ROOT).hashCode() : "" + e.getUniqueId().hashCode(), entity_netadata);
            }
            
            // restore netadata for offline players
            for (Entry<String, Map<String, Object>> offline_root_netadata_entry : OfflineNetadata.offlineNetadata.entrySet()) {
                it = offline_root_netadata_entry.getValue().entrySet().iterator();
                Loop_Player: while (it.hasNext()) {
                    Entry<String, Object> se = it.next();
                    if (!(se.getValue() instanceof NetadataValue)) {
                        it.remove();
                        continue;
                    }
                    Object realValue;
                    for (Neta neta : ((NetadataValue) se.getValue()).neta) {
                        if (neta != Neta.PERSISTENT) continue;
                        realValue = ((NetadataValue) se.getValue()).value;
                        if (!(realValue instanceof Serializable)) {
                            it.remove();
                            continue Loop_Player;
                        }
                        se.setValue(realValue); // remember to re-wrap on loading
                        continue Loop_Player;
                    }
                    it.remove(); // entry gets here is non-persistent
                }
                worldNetadata.put("ID" + offline_root_netadata_entry.getKey(), offline_root_netadata_entry.getValue());
            }
            
            if (worldNetadata.isEmpty()) continue; // next world
            Output: {
                try {
                    FileOutputStream fos = new FileOutputStream(dataFile);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(worldNetadata);
                    oos.flush(); fos.flush();
                    oos.close(); fos.close();
                } catch (IOException io) {
                    io.printStackTrace();
                    Bukkit.getLogger().warning("NetadataEngine > 存储 Netadata 数据时遇到问题 : 正重试");
                    coffee(5);
                    break Output;
                }
            }
        }
        Bukkit.getLogger().warning("NetadataEngine > 数据存储完成");
    }

}
