package moe.kira.netadata;

import java.util.Arrays;

import javax.annotation.Nullable;

public class NetadataValue {
    Object value;
    Neta[] neta;
    NetadataValue() {}
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(neta);
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        NetadataValue other = (NetadataValue) obj;
        if (!Arrays.equals(neta, other.neta)) return false;
        if (value == null) {
            if (other.value != null) return false;
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    public NetadataValue(Object value) {
        this.value = value;
    }
    
    public NetadataValue(Object value, Neta... neta) {
        this.value = value;
        this.neta = neta;
    }
    
    public static NetadataValue wrap(Object value) {
        return new NetadataValue(value);
    }
    
    public static NetadataValue wrap(Object value, Neta... neta) {
        return new NetadataValue(value, neta);
    }

    /**
     * Fetches the value of this netadata item.
     *
     * @return the netadata value.
     */
    public Object value() {
        return value;
    }

    /**
     * Attempts to convert the value of this netadata item into an int.
     *
     * @return the value as an int.
     */
    public int asInt() {
        return (int) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a float.
     *
     * @return the value as a float.
     */
    public float asFloat() {
        return (float) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a double.
     *
     * @return the value as a double.
     */
    public double asDouble() {
        return (double) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a long.
     *
     * @return the value as a long.
     */
    public long asLong() {
        return (long) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a short.
     *
     * @return the value as a short.
     */
    public short asShort() {
        return (short) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a byte.
     *
     * @return the value as a byte.
     */
    public byte asByte() {
        return (byte) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a boolean.
     *
     * @return the value as a boolean.
     */
    public boolean asBoolean() {
        return (boolean) value;
    }

    /**
     * Attempts to convert the value of this netadata item into a string.
     *
     * @return the value as a string.
     */
    public String asString() {
        return (String) value;
    }

    @Nullable
    public Neta[] neta() {
        return neta;
    }
}
